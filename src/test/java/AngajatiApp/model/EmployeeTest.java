package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import org.junit.Test;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeTest {
    Employee employee = new Employee("a","b","1111111111111",DidacticFunction.ASISTENT, 3000.0);

    @BeforeEach
    void setUp() {
        System.out.println("START");
    }

    @AfterEach
    void tearDown() {
        System.out.println("FINISH");
    }

    @Test
    void constructorShouldSetAllFieldsCorrectly() {
        assertEquals("firstName1", employee.getFirstName());
        assertEquals("name1", employee.getLastName());
        assertEquals("1111111111111", employee.getCnp());
        assertEquals(DidacticFunction.ASISTENT, employee.getFunction());
        assertEquals(3000.0, employee.getSalary());
    }

    @Test
    void setFirstNameShouldSetCorrectFirstName() {
        String setFirstName = "aa";
        employee.setFirstName(setFirstName);
        assertEquals("aa",employee.getFirstName());
    }

    @Test
    void setCnpShouldSetCorrectCnp() {
        String setCnp = "2222222222222";
        employee.setCnp(setCnp);
        assertEquals("2222222222222",employee.getCnp());
    }

    @Test
    void setDidacticFunctionShouldSetCorrectFunction() {
        DidacticFunction setDidacticFunction = DidacticFunction.LECTOR;
        employee.setFunction(setDidacticFunction);
        assertEquals(DidacticFunction.LECTOR,employee.getFunction());
    }

    @Test
    void getFirstNameShouldReturnCorrectFirstName() {
        assert(employee.getFirstName().equals("a") );
    }

    @Test
    void getSalaryShouldReturnCorrectSalary() {
        assert(employee.getSalary().equals(3000.0) );
    }

    //timeout
    @Test(timeout=1000)
    void setSalaryShouldSetCorrectSalaryWithTimeout() {
        Double setSalary = 4000.0;
        employee.setSalary(setSalary);
        assertEquals(4000.0,employee.getSalary());
    }

    //order

//    @TestMethodOrder(OrderAnnotation.class)
        @Test
        @Order(1)
        void getNameShouldReturnCorrectName() {
            assert (employee.getLastName().equals("b"));
            System.out.println("test1");
        }

        @Test
        @Order(2)
        void getDidacticFunctionShouldReturnCorrectFunction() {
            assert (employee.getFunction().equals(DidacticFunction.ASISTENT));
            System.out.println("test2");
        }

    //test parametrizat
    @ParameterizedTest
    @ValueSource (strings ={"firstName1","firstName2", "firstName3"})
    void getFirstNameShouldReturnCorrectFirstNameParam(String firstName){
        Employee employee = new Employee();
        employee.setFirstName("firstName2");
        assertEquals("firstName2",employee.getFirstName());
        System.out.println("Titlul " + firstName + " este corect");
    }

    //exception
    @Test
    void setCnpShouldSetCorrectCnpException() {
        try {
            String setCnp = "2222222222222";
            employee.setCnp(setCnp);
            assertEquals("2222222222222",employee.getCnp());
        }
        catch (Exception e) {
            e.printStackTrace();
            assert(e.getMessage().length()>0);
        }
    }
}